# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

PYTHON_DEPEND="2"

inherit eutils python

MY_PN="xplanetFX"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="Utility for rendering high quality wallpapers of mother earth on your desktop"
HOMEPAGE="http://mein-neues-blog.de/category/xplanetfx/"
SRC_URI="http://repository.mein-neues-blog.de:9000/archive/${P}_all.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"
IUSE="gtk perl"

RDEPEND="|| ( media-gfx/imagemagick[perl?] media-gfx/graphicsmagic[perl?] )
	x11-misc/xplanet
	dev-perl/libwww-perl
	gtk? ( dev-python/pygtk:2 )
	sys-devel/bc"
DEPEND=""

S="${WORKDIR}"

pkg_setup() {
	python_set_active_version 2
	python_pkg_setup
}

src_prepare() {
	epatch "${FILESDIR}"/${P}-gentoo.patch
	# fix gtk script
	if use gtk; then
		sed -i -e "/gtk/s:\$WORKDIR\/::" ${S}/usr/bin/${MY_PN}
		# fix paths
		sed -i -e "s:sys\.path\[0\]:\"\/usr/share/${MY_PN}\":g" \
			${S}/usr/share/${MY_PN}/${MY_PN}_gtk
		# fix python sitedir
		sed -i -e "s:import flipview:import ${PN}\.flipview as flipview:" \
			"${S}"/usr/share/${MY_PN}/${MY_PN}_gtk
		# fix language imports
		sed -i -e "/ st =/s:languages:${PN}\.languages:" \
			"${S}"/usr/share/${MY_PN}/${MY_PN}_gtk
	fi
}

src_install() {
	dobin usr/bin/${MY_PN} usr/share/${MY_PN}/${MY_PN}_gtk
	rm usr/share/${MY_PN}/${MY_PN}_gtk
	insinto $(python_get_sitedir)/${PN}
	touch ${D}/$(python_get_sitedir)/${PN}/__init__.py
	doins usr/share/${MY_PN}/flipview.py
	doins -r usr/share/${MY_PN}/languages
	rm usr/share/${MY_PN}/flipview.py
	rm -r usr/share/${MY_PN}/languages/
	insinto /usr/share/pixmaps
	doins usr/share/pixmaps/${MY_PN}.svg
	insinto /usr/share/applications/
	doins usr/share/applications/${MY_PN}.desktop
	insinto /usr/share/${MY_PN}/
	doins -r usr/share/${MY_PN}/*
}

pkg_postinst() {
	python_mod_optimize $(python_get_sitedir)/${PN}
}

pkg_postrm() {
	python_mod_cleanup $(python_get_sitedir)/${PN}
}
